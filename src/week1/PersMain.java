package week1;

public class PersMain {
    public static void main(String[] args) {
        Person person1 = new Person();
        Person person2 = new Person("Antony",1996 );
        Person person3 = new Person("Alexander", 1998);
        Person person4 = new Person("Lauren", 1986);
        Person person5 = new Person();
        Person person6 = new Person();

        person1.setName("Angeline");
        person5.setName("Gary");

        person1.setBirthYear(2000);
        person5.setBirthYear(1998);

        person1.age();
        System.out.println(person1.age());
        person2.setName("Denis");
        person2.output();
        System.out.println(person2.output());





    }
}
