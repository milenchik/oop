package week1;

public class Main {
    public static void main(String[] args) {
        Employee employee1 = new Employee();
        Employee employee2 = new Employee("Svetlana", 12, 7);
        Employee employee3 = new Employee("Anna", 15);

        employee1.setName("Pavel");
        employee1.setHours(10);
        employee1.setRate(5.5);
        employee3.setHours(8);
        System.out.println(employee1);
        System.out.println(employee2);
        System.out.println(employee3);
        employee1.salary(employee1.getRate(), employee1.getHours());
        employee2.salary(employee2.getRate(), employee2.getHours());
        employee3.bonuses();
        employee2.setRate(16);
        System.out.println(employee2);
        System.out.println(employee1.salary(employee1.getRate(), employee1.getHours()));

        System.out.println(Employee.totalHours);
    }
}
