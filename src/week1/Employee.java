package week1;

public class Employee {
    private String name;
    private double rate;
    private int hours;
    static int totalSum;
    static int totalHours=0;

    public Employee(){}

    public Employee(String name, double rate){
        this.name=name;
        this.rate=rate;
    }
    public Employee (String name, double rate, int hours){
        this.name=name;
        this.rate=rate;
        this.hours=hours;
        totalHours+=hours;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public int getHours() { return hours; }

    public void setHours(int hours) {
        this.hours = hours;
        totalHours = totalHours+hours;
    }


    public double salary(double rate, int hours){
        double calcSal=rate*hours;
        return calcSal;
    }
    public String toString(){
        return "Employee [name=" + name +", hours=" + hours +", rate= "+ rate +"]";
    }

    public double bonuses(){
        double calcBonus= 0.1*salary(rate, hours);
        return calcBonus;
    }
}
