package week2;

public interface WeaponBehavior {
    void useWeapon();
}
